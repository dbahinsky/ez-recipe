import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import dishes from '../../public/data.json'

export const store = new Vuex.Store({
    state: {
        dishes,
        selectedType: '',
        favorites: []
    },
    getters: {
        getDishes(state) {
            const dishesId = state.dishes.map((i, index) => {
                i.id = index
                return i
            })
            if (state.selectedType) {
                return dishesId.filter(i => (i.categorie_id === state.selectedType))
            } else {
                return dishesId
            }
        },
        getDishType(state) {
            return state.selectedType
        },
        getFavoritesCount(state) {
            return state.dishes.filter(i => i.favorite === true).length
        }
    },
    mutations: {
        selectDishType(state, payload) {
            if (state.selectedType === payload) {
                state.selectedType = ''
            } else {
                state.selectedType = payload
            }
        },
        addToFavorite(state, payload) {
            state.dishes[payload].favorite = !state.dishes[payload].favorite
        }
    }
})